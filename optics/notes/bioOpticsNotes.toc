\contentsline {chapter}{Chapter 1 - Introduction}{5}{chapter*.2}
\contentsline {chapter}{Chapter 2 - Optical Absorption}{7}{chapter*.3}
\contentsline {chapter}{Chapter 3 - Optical Scattering}{9}{chapter*.4}
\contentsline {chapter}{Chapter 4 - Light Propagation in Tissue}{11}{chapter*.5}
\contentsline {section}{\textbf {Terminology}}{11}{section*.6}
\contentsline {subsection}{Radiance}{11}{subsection*.7}
\contentsline {subsection}{Flux Vector and Fluence Rate}{11}{subsection*.8}
\contentsline {subsection}{Absorbed Power and Energy Densities}{11}{subsection*.9}
\contentsline {section}{\textbf {Radiative Transfer Equation}}{11}{section*.10}
\contentsline {section}{\textbf {Beer-Lamber-Bouguer Law}}{13}{section*.11}
\contentsline {section}{\textbf {Diffuse Approximation}}{13}{section*.12}
\contentsline {subsection}{P$_1$ Approximation}{13}{subsection*.13}
\contentsline {subsection}{Diffusion Approximation}{14}{subsection*.14}
\contentsline {section}{Monte Carlo Simulation}{16}{section*.15}
\contentsline {chapter}{Chapter 5 - De-excitation Pathways and Bioeffects}{17}{chapter*.16}
\contentsline {chapter}{Bibliography}{19}{chapter*.17}
\contentsline {chapter}{Index}{21}{chapter*.18}
\contentsfinish 
