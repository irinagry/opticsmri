\contentsline {chapter}{The Design of Tufte's Books}{7}{chapter*.3}
\contentsline {section}{Typefaces}{11}{section*.4}
\contentsline {section}{Headings}{11}{section*.5}
\contentsline {paragraph}{Paragraph}{11}{paragraph*.6}
\contentsline {section}{Environments}{11}{section*.7}
\contentsline {chapter}{On the Use of the tufte-book Document Class}{13}{chapter*.8}
\contentsline {section}{Page Layout}{13}{section*.9}
\contentsline {subsection}{Headings}{13}{subsection*.10}
\contentsline {section}{Sidenotes}{13}{section*.11}
\contentsline {section}{References}{14}{section*.12}
\contentsline {section}{Figures and Tables}{15}{section*.13}
\contentsline {section}{Captions}{17}{section*.14}
\contentsline {paragraph}{Vertical alignment}{17}{paragraph*.15}
\contentsline {paragraph}{Horizontal alignment}{17}{paragraph*.16}
\contentsline {section}{Full-width text blocks}{18}{section*.17}
\contentsline {section}{Typography}{18}{section*.18}
\contentsline {subsection}{Typefaces}{18}{subsection*.19}
\contentsline {subsection}{Letterspacing}{18}{subsection*.20}
\contentsline {section}{Document Class Options}{19}{section*.21}
\contentsline {chapter}{Customizing Tufte-LaTeX}{21}{chapter*.22}
\contentsline {section}{File Hooks}{21}{section*.23}
\contentsline {section}{Numbered Section Headings}{21}{section*.24}
\contentsline {section}{Changing the Paper Size}{22}{section*.25}
\contentsline {section}{Customizing Marginal Material}{22}{section*.26}
\contentsline {chapter}{Compatibility Issues}{25}{chapter*.27}
\contentsline {section}{Converting from \texttt {article} to \texttt {tufte-handout}}{25}{section*.28}
\contentsline {section}{Converting from \texttt {book} to \texttt {tufte-book}}{25}{section*.29}
\contentsline {chapter}{Troubleshooting and Support}{27}{chapter*.30}
\contentsline {section}{Tufte-\LaTeX \xspace Website}{27}{section*.31}
\contentsline {section}{Tufte-\LaTeX \xspace Mailing Lists}{27}{section*.32}
\contentsline {paragraph}{Discussion list}{27}{paragraph*.33}
\contentsline {paragraph}{Commits list}{27}{paragraph*.34}
\contentsline {section}{Getting Help}{27}{section*.35}
\contentsline {section}{Errors, Warnings, and Informational Messages}{28}{section*.36}
\contentsline {section}{Package Dependencies}{28}{section*.37}
\contentsline {chapter}{Bibliography}{31}{chapter*.38}
\contentsline {chapter}{Index}{33}{chapter*.39}
\contentsfinish 
